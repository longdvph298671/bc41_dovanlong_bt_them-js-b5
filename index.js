
/**
 * bài 1
 */
var result = document.getElementById("result");
function tinhThue(){
    var hoTen = document.getElementById("ho-ten").value;
    var tongThuNhap = document.getElementById("tong-thu-nhap").value*1;
    var soNguoi = document.getElementById("so-phu-thuoc").value*1;
    var thue = 0;
    var thuNhapChiuThue = tongThuNhap - 4e+6 - (soNguoi * 16e+5);
    if(tongThuNhap <= 4e+6){
        alert("Số tiền thu nhập không hợp lệ");
    }
    else{
        if(thuNhapChiuThue <= 60e+6) {
            thue = thuNhapChiuThue * 0.05;
        }
        else if(thuNhapChiuThue <= 120e+6) {
            thue = 60e+6 * 0.05 + (thuNhapChiuThue - 60e+6) * 0.1;
        }
        else if(thuNhapChiuThue <= 210e+6) {
            thue = 60e+6 * 0.05 + 60e+6 * 0.1 + (thuNhapChiuThue - 120e+6) * 0.15;
        }
        else if(thuNhapChiuThue <= 384e+6) {
            thue = 60e+6 * 0.05 + 60e+6 * 0.1 + 90e+6 * 0.15 + (thuNhapChiuThue - 210e+6) * 0.2;
        }
        else if(thuNhapChiuThue <= 624e+6) {
            thue = 60e+6 * 0.05 + 60e+6 * 0.1 + 90e+6 * 0.15 + (384e+6 - 210e+6) * 0.2 + (thuNhapChiuThue - 384e+6) * 0.25;
        }
        else if(thuNhapChiuThue <= 960e+6) {
            thue = 60e+6 * 0.05 + 60e+6 * 0.1 + 90e+6 * 0.15 + (384e+6 - 210e+6) * 0.2 + (624e+6 - 384e+6) * 0.25 + (thuNhapChiuThue - 624e+6) * 0.3;
        }
        else {
            thue = 60e+6 * 0.05 + 60e+6 * 0.1 + 90e+6 * 0.15 + (384e+6 - 210e+6) * 0.2 + (624e+6 - 384e+6) * 0.25 + (960e+6 - 624e+6) * 0.3 + (thuNhapChiuThue - 960e+6) * 0.35;
        }
        result.innerHTML = `<h3>${new Intl.NumberFormat().format(thue)} VND</h3>`;
    }
}
/**
 * bài 2
 */
var loaiKhachHang = document.getElementById("loai-khach-hang");
var maKhachHang = document.getElementById("ma-khach-hang");
var soKenh = document.getElementById("so-kenh");
var soKetNoi = document.getElementById("so-ket-noi");
function chonKhachHang(){
    if(loaiKhachHang.value == 2){
        soKetNoi.style.display = "block";
    }
    else {
        soKetNoi.style.display = "none";
    }
}
function phiDichVuDoanhNghiep(soKetNoi){
    if(soKetNoi <= 10){
        return 75;
    }
    else {
        return (75 + (soKetNoi - 10)*5);
    }
}
function tinhTienCap() {
    var tongTienCap = 0;
    if(loaiKhachHang.value == 1){
        tongTienCap = 4.5 + 20.5 + Number(soKenh.value) * 7.5;
        result.innerHTML = `<h3>$${new Intl.NumberFormat().format(tongTienCap)}</h3>`;
    }
    else if(loaiKhachHang.value == 2){
        console.log(phiDichVuDoanhNghiep(soKetNoi.value*1));
        tongTienCap = 15 + phiDichVuDoanhNghiep(soKetNoi.value*1) + Number(soKenh.value) * 50;
    }
    else {
        alert("Hãy chọn loại khách hàng");
    }
    result.innerHTML = `<h3>Mã khách hàng: ${maKhachHang.value}; Tiền cáp: ${new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD'}).format(tongTienCap)}</h3>`;
}

